# Protocolos para la transmisión de audio y video en Internet
# Práctica 5. Sesión SIP

## Introducción

El protocolo de iniciación de sesión (SIP) es un protocolo que se limita solamente al establecimiento y control de una sesión ([RFC 3261](https://www.rfc-editor.org/rfc/rfc3261)). Los detalles del intercambio de datos, como por ejemplo la codificación o decodificación del audio/vídeo, no son controlados por SIP sino que se llevan a cabo por otros protocolos (por ejemplo, [RTP](https://www.rfc-editor.org/rfc/rfc3550.html)). Los principales objetivos de SIP son:

* SIP permite el establecimiento de una localización de usuario (o sea, traducir de un nombre de usuario a su dirección de red actual)
* SIP provee funcionalidad para la negociación de las características de una sesión, de manera que los participantes en una sesión pueden consensuar características soportadas por todos ellos.
* SIP es un mecanismo para la gestión de llamadas, por ejemplo para añadir, eliminar o transferir participantes
* SIP permite la modificación de características de la sesión durante su transcurso.

## Objetivos de la práctica

* Conocer el protocolo SIP y otros protocolos utilizados en una sesión con clientes SIP (como _Linphone_).
* Profundizar en el uso de _Wireshark_: análisis, captura y filtrado.

## Ejercicio 1. Creación de repositorio para la práctica

Con el navegador, dirígete al repositorio plantilla de esta práctica y realiza un fork, de manera que consigas tener una copia del repositorio en tu cuenta de GitLab. Clona el repositorio que acabas de crear a local para poder editar los archivos. Trabaja a partir de ahora en ese repositorio, sincronizando los cambios que vayas realizando según los ejercicios que se comentan a continuación (haciendo commit y subiéndolo a tu repositorio en el GitLab de la ETSIT).

## Ejercicio 2. Prepara la respuesta

Se ha capturado una sesión SIP con el cliene SIP Linphone (archivo `linphone-call.pcapng`), que se puede abrir con _Wireshark_ (desde la shell, `wireshark linphone-call.pcapng`). Se pide rellenar las cuestiones que se plantean en este guión en el fichero `respuesta.md`, que crearás en el repositorio (y que subiŕas tu repositorio en el GitLab de la ETSIT para su corrección).

Para crear el fichero `respuesta.md`, copia este documento, en formato Markdown, desde el ejercicio siguiente (incluido) en adelante, de forma que tengas los enunciados de las preguntas en él, y responde a cada una de las preguntas abriendo, tras el ejercicio correspondiente, una sección `## Respuesta n` donde escribas la respuesta.

## Ejercicio 3. Análisis general. 
### Todas las respuestas se encuentran a continuación de cada pregunta

* ¿Cuántos paquetes componen la captura?     
1050 paquetes
* ¿Cuánto tiempo dura la captura?    
Aproximadamente 10 segundos y medio
* ¿Qué IP tiene la máquina donde se ha efectuado la captura?    
192.168.1.116
* ¿Se trata de una IP pública o de una IP privada?    
La IP origen es una IP privada, y la IP destino es una IP pública
* ¿Por qué lo sabes?    
Las direcciones IP privadas suelen comenzar por 10, 172 o 192

Antes de analizar las tramas, mira las estadísticas generales que aparecen en el menú de `Statistics`. En el apartado de jerarquía de protocolos (`Protocol Hierarchy`) se puede ver el número de paquetes y el tráfico correspondiente a los distintos protocolos.

* ¿Cuáles son los tres principales protocolos de nivel de aplicación por número de paquetes?   
RTP(UDP), SIP(UDP), RTP(ICMP) 
* ¿Qué otros protocolos podemos ver en la jerarquía de protocolos?   
IPv4(Ethernet), IPv4 siendo el protocolo de nivel y Ethernet el de enlace. También se encuentran UDP como protocolo de transporte y ICMP de internet
* ¿Qué protocolo de nivel de aplicación presenta más tráfico, en bytes por segundo? ¿Cuánto es ese tráfico?   
RTP(UDP), 134 Kbits/s, es decir, un 98.2% de paquetes

Observa por encima el flujo de tramas en el menú de `Statistics` en `IO Graphs`. La captura que estamos viendo corresponde con una llamada SIP.

Filtra por `sip` para conocer cuándo se envían paquetes SIP, o por 'rtp', para conocer cuándo se envían los RTP.

* ¿En qué segundos tienen lugar los dos primeros envíos SIP?   
Los dos al comienzo de la captura, es decir, en el segundo 0
* Y los paquetes con RTP, ¿en qué paquete de la trama se empiezan a enviar?   
En el 6º paquete
* Los paquetes RTP, ¿cada cuánto se envían?   
Aproximadamente cada 0.01 segundos

Sigue respondiendo las cuestiones que se plantean en este guión en el fichero `respuesta.md` (respondiendo en una nueva sección de respuesta, como se ha indicado).

Al terminar el ejercicio es recomendable hacer commit de los ficheros modificados.

## Ejercicio 4. Primeras tramas

Analiza ahora las dos primeras tramas de la captura. Cuando se pregunta por máquinas, llama "Linphone" a la máquina donde está funcionando el cliente SIP, y que funcionará por lo tanto como UA (user agent), y "Servidor" a la máquina que proporciona el servicio SIP. Todas las pregntas de este ejercicio se refieren a la captura cuando está filtrada de forma que sólo se ven tramoas del protocolo SIP.

* ¿De qué protocolo de nivel de aplicación son?   
Del protocolo SIP
* ¿Cuál es la dirección IP de la máquina "Linphone"?   
192.168.1.116
* ¿Cuál es la dirección IP de la máquina "Servidor"?   
212.79.111.155
* ¿Que ha ocurrido (en Linphone o en Servidor) para que se envíe la primera trama?   
Linphone le envía un request de invitación al servidor
* ¿Qué ha ocurrido (en Linphone o en Servidor) para que se envíe la segunda trama?   
El servidor le contesta con un trying, es decir, intenta establecer conexión

Ahora, veamos las dos tramas siguientes.

* ¿De qué protocolo de nivel de aplicación son?   
Del protocolo SIP
* ¿Entre qué máquinas se envía cada trama?   
En el 4ª paquete de la dirección 212.79.111.155 (servidor) a la 192.168.1.116 (cliente), y en el 5ª paquete al revés
* ¿Que ha ocurrido para que se envíe la primera de ellas (tercera trama en la captura)?   
El servidor termina estableciendo conexión con el 200 OK
* ¿Qué ha ocurrido para que se envíe la segunda de ellas (cuarta trama en la captura)?   
El cliente le responde con un ACK, para que sepa el servidor que lo ha recibido

Sigue respondiendo las cuestiones que se plantean en este guión en el fichero `respuesta.md` (respondiendo en una nueva sección de respuesta, como se ha indicado).
Al terminar el ejercicio es recomendable hacer commit de los ficheros modificados.

## Ejercicio 5. Tramas finales

Después de la trama 250, busca la primera trama SIP.

* ¿Qué número de trama es?   
Es la trama 12 de SIP y el número de paquete 1042
* ¿De qué máquina a qué máquina va?   
De la dirección 192.168.1.116 (cliente) a la 212.79.111.155 (servidor)
* ¿Para qué sirve?   
Es un request BYE de fin de conexión 
* ¿Puedes localizar en ella qué versión de Linphone se está usando?    
Linphone Desktop/4.3.2 (Debian GNU/Linux bookworm/sid, Qt 5.15.6) LinphoneCore/5.0.37

Sigue respondiendo las cuestiones que se plantean en este guión en el fichero `respuesta.md` (respondiendo en una nueva sección de respuesta, como se ha indicado).
Al terminar el ejercicio es recomendable hacer commit de los ficheros modificados.

## Ejercicio 6. Invitación

Seguimos centrados en los paquetes SIP. Veamos en particular el primer paquete (INVITE)

* ¿Cuál es la direccion SIP con la que se quiere establecer una llamada?   
music@sip.iptel.org
* ¿Qué instrucciones SIP entiende el UA?
Según muestra el Allow: INVITE, ACK, CANCEL, OPTIONS, BYE, REFER,  NOTIFY, MESSAGE, SUBSCRIBE, INFO, PRACK, UPDATE.
* ¿Qué cabecera SIP indica que la información de sesión va en formato SDP?   
Content-type
* ¿Cuál es el nombre de la sesión SIP?   
Talk

## Ejercicio 7. Indicación de comienzo de conversación

En la propuesta SDP de Linphone puede verse un campo `m` con un valor que empieza por `audio 7078`.

* ¿Qué trama lleva esta propuesta?   
La primera trama de SIP y el segundo paquete de la captura total
* ¿Qué indica el `7078`?    
El puerto multimedia
* ¿Qué relación tiene con el destino de algunos paquetes que veremos más adelante en la trama?   
Que irán por ese mismo puerto (7078) de cliente a servidor
* ¿Qué paquetes son esos?   
Los paquetes RTP que se enviarán, más concretamente audio

En la respuesta a esta propuesta vemos un campo `m` con un valor que empieza por `audio XXX`.

* ¿Qué trama lleva esta respuesta?   
La tercera trama de SIP y el cuarto paquete de la captura total
* ¿Qué valor es el `XXX`?   
29448
* ¿Qué relación tiene con el destino de algunos paquetes que veremos más adelante en la trama?   
Que irán por ese mismo puerto (29448) de servidor a cliente
* ¿Qué paquetes son esos?   
Los paquetes RTP que se enviarán, más concretamente audio

Sigue respondiendo las cuestiones que se plantean en este guión en el fichero `respuesta.md` (respondiendo en una nueva sección de respuesta, como se ha indicado).
Al terminar el ejercicio es recomendable hacer commit de los ficheros modificados.

## Ejercicio 8. Primeros paquetes RTP

Vamos ahora a centrarnos en los paquetes RTP. Empecemos por el primer paquete RTP:

* ¿De qué máquina a qué máquina va?   
De la dirección 192.168.1.116 (cliente) a la 212.79.111.155 (servidor)
* ¿Qué tipo de datos transporta?   
Flujo Multimedia
* ¿Qué tamaño tiene?   
214 bytes
* ¿Cuántos bits van en la "carga de pago" (payload)?   
172 bytes, es decir, 1376 bits
* ¿Cuál es la periodicidad de los paquetes según salen de la máquina origen?   
Aproximadamente cada 0.01 segundos

Sigue respondiendo las cuestiones que se plantean en este guión en el fichero `respuesta.md` (respondiendo en una nueva sección de respuesta, como se ha indicado).
Al terminar el ejercicio es recomendable hacer commit de los ficheros modificados.

## Ejercicio 9. Flujos RTP

Vamos a ver más a fondo el intercambio RTP. Busca en el menú `Telephony` la opción `RTP`. Empecemos mirando los flujos RTP.

* ¿Cuántos flujos hay? ¿por qué?   
Hay dos flujos RTP, uno de cliente a servidor y otro de servidor a cliente
* ¿Cuántos paquetes se pierden?   
No se pierde ningún paquete
* Para el flujo desde LinPhone hacia Servidor: ¿cuál es el valor máximo del delta?   
30,7267 ms
* ¿Qué es lo que significa el valor de delta?   
La periodicidad o diferencia del tiempo de envio entre paquete y paquete
* ¿En qué flujo son mayores los valores de jitter (medio y máximo)?   
En el del servidor al linphone
* ¿Qué significan esos valores?   
El jitter es la variabilidad temporal durante el envío

Vamos a ver ahora los valores de una trama concreto, la númro 27. Vamos a analizarla en opción `Telephony`, `RTP`, `RTP Stream Analysis`:

* ¿Cuánto valen el delta y el jitter para ese paquete?   
Delta 0.000164 y jitter 3.087182
* ¿Podemos saber si hay algún paquete de este flujo, anterior a este, que aún no ha llegado?   
Si, mirando el valor Skew
* El "skew" es negativo, ¿qué quiere decir eso?   
Que dicho paquete ha llegado 4.527123 ms antes

En el panel `Stream Analysis` puedes hacer `play` sobre los streams:

* ¿Qué se oye al pulsar `play`?   
Una melodía instrumental
* ¿Qué se oye si seleccionas un `Jitter Buffer` de 1, y pulsas `play`?   
Se escucha el audio entrecortado
* ¿A qué se debe la diferencia?   
Que cuando el jitter es más pequeño se espera durante menos tiempo a los paquetes
enviados, por tanto, produce más cortes en el audio. 

Sigue respondiendo las cuestiones que se plantean en este guión en el fichero `respuesta.md` (respondiendo en una nueva sección de respuesta, como se ha indicado).
Al terminar el ejercicio es recomendable hacer commit de los ficheros modificados.
  
## Ejercicio 10. Llamadas VoIP

Podemos ver ahora la traza, analizándola como una llamada VoIP completa. Para ello, en el menú `Telephony` selecciona el menú `VoIP calls`, y selecciona la llamada hasta que te salga el panel correspondiente:

* ¿Cuánto dura la llamada?   
10 segundos

Ahora, pulsa sobre `Flow Sequence`:

* Guarda el diagrama en un fichero (formato PNG) con el nombre `diagrama.png`.
* ¿En qué segundo se recibe el último OK que marca el final de la llamada?   
En el 10.5216
  
Ahora, selecciona los dos streams, y pulsa sobre `Play Sterams`, y volvemos a ver el panel para ver la transmisión de datos de la llamada (protocolo RTP):

* ¿Cuáles son las SSRC que intervienen?   
Intervienen dos, una en el primer flujo de cliente a servidor 0x0D2DB8B4 y 
otra en el segundo flujo de servidor a cliente 0x5C44A34B 
* ¿Cuántos paquetes se envían desde LinPhone hasta Servidor?   
113, 2 y 399 por partes, lo que hacen un total de 514 paquetes
* ¿Cuál es la frecuencia de muestreo del audio?   
8 KHz (Sample Rate)
* ¿Qué formato se usa para los paquetes de audio (payload)?   
g711U

Sigue respondiendo las cuestiones que se plantean en este guión en el fichero `respuesta.md` (respondiendo en una nueva sección de respuesta, como se ha indicado).
Al terminar el ejercicio es recomendable hacer commit de los ficheros modificados.

## Ejercicio 11. Captura de una llamada VoIP

Desde LinPhone, créate una cuenta SIP.  A continuación:

* Captura una llamada VoIP con el identificador SIP `sip:music@sip.iptel.org`, de unos 10 segundos de duración. Comienza a capturar tramas antes de arrancar LinPhone para ver todo el proceso. Guarda la captura en el fichero `linphone-music.pcapng`. Asegura (usando los filtros antes de guardarla) que en la captura haya solo paquetes entre la máquina donde has hecho la captura y has ejecutado LinPhone, y la máquina o máquinas que han intervenido en los intercambios SIP y RTP con ella.
* ¿Cuántos flujos RTP tiene esta captura?   
Tiene un solo flujo RTP desde mi máquina a la máquina servidor, ya que 
la máquina servidor no responde a mi llamada
* ¿Cuánto es el valor máximo del delta y los valores medios y máximo del jitter de cada uno de los flujos?   
No puedo ver estos valores debido a no recibir respuesta del servidor.
De todas formas las podría encontrar en cualquier trama RTP en la opción 
Telephony, RTP, RTP Stream Analysis

Sigue respondiendo las cuestiones que se plantean en este guión en el fichero `respuesta.md` (respondiendo en una nueva sección de respuesta, como se ha indicado).
Al terminar el ejercicio es recomendable hacer commit de los ficheros modificados.

## ¿Qué se valora de esta la práctica?

Valoraremos de esta práctica sólo lo que esté en la rama principal de
tu repositorio, creado de la forma que hemos indicado (como fork del repositorio plantilla que os proporcionamos). Por lo tanto, aségurate de que está en él todo lo que has realizado.

Además, ten en cuenta:

* Se valorará que haya realizado al menos haya ocho commits, correspondientes más o menos con los ejercicios pedidos, en al menos dos días diferentes, sobre la rama principal del repositorio.
* Se valorará que el fichero de respuestas esté en formato Markdown correcto.
* Se valorará que esté el texto de todas las preguntas en el fichero de respuestas, tal y como se indica al principio de este enunciado.
* Se valorará que las respuestas sean fáciles de entender, y estén correctamente relacionadas con las preguntas.
* Se valorará que la captura que se pide tenga exactamente lo que se pide.
* Se valorará que el fichero con el diagrama de la llamada VoIP tenga solo ese diagrama, en el formato que se indica en el enunciado.
* Parte de la corrección será automática, así que asegúrate de que los nombres que utilizas para los archivos son los mismos que indica el enunciado.

## ¿Cómo puedo probar esta práctica?

Cuando tengas la práctica lista, puedes realizar una prueba general, de que los ficheros en el directorio de entrega son los adecuados, y alguna otra comprobación. Para ello, ejecuta el archivo `check.py`, bien en PyCharm, o bien desde la línea de comandos:

```shell
python3 check.py
```

## Ejercicio 12 (segundo periodo). Llamada LinPhone

Ponte de acuerdo con algún compañero para realizar una llamada SIP conjunta (entre los dos) usando LinPhone, de unos 15 segundos de duración.  Si quieres, usa el foro de la asignatura, en el hilo correspondiente a este ejercicio, para enconrar con quién hacer la llamada.

Realiza una captura de esa llamada en la que estén todos los paquetes SIP y RTP (y sólo los paquetes SIP y RTP). No hace falta que tu compañero realice captura (pero puede realizarla si quiere que le sirva también para este ejercicio).

Guarda tu captura en el fichero `captura-a-dos.pcapng`

Como respuesta a este ejercicio, indica con quién has realizado la llamada, quién de los dos la ha iniciado, y cuantos paquetes hay en la captura que has realizado.